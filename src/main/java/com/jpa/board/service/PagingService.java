/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.jpa.board.service;

import com.jpa.board.dto.BoardDTO;
import com.jpa.board.dto.PagingDTO;
import java.util.ArrayList;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

/**
 *
 * @author qntjd
 * 페이징 처리를 위한 클래스
 */
@Service
@RequiredArgsConstructor
public class PagingService {
    private final BoardService boardService;
    int pageLimit = 10;//한 페이지에 보여질 개수
    int blockLimit= 3;
    
    /**
     * 
     * @param pageable
     * @return 
     * 게시글 리스트를 페이징 처리하는 함수
     */
    public PagingDTO getPaingBoardList(Pageable pageable){
        int startPage; // 하단에 페이지를 선택할 수 있도록하는 목록의 시작
        int endPage; // 하단에 페이지를 선택할 수 있도록하는 목록의 종료
        boolean pre = true;
        boolean next = true;
        Page<BoardDTO> pagelist = boardService.findAll(pageable, pageLimit);
        if(pageable.getPageNumber() <= blockLimit){ // 현재 페이지가 보여질 페이질 목록 수보다 작을 때
            startPage = 1;
            if(pagelist.getTotalPages()<blockLimit){ // 총 페이지 수가 페이지 목록 수 보다 작을 때
                endPage = pagelist.getTotalPages();
            }else{
                endPage = blockLimit;
            }
        }else if(pageable.getPageNumber() >= (pagelist.getTotalPages()-blockLimit+1)){// 현재 페이지가 마지막 페이지 목록에 위치해 있을 때
            startPage = pagelist.getTotalPages()- blockLimit+1;
            endPage = pagelist.getTotalPages();
        }else{
            startPage = pageable.getPageNumber()-(blockLimit/2);
            endPage = pageable.getPageNumber()+(blockLimit/2);
        }
        if(pagelist.isFirst()){
            pre = false;
        }
        if(pagelist.isLast()){
            next = false;
        }
        return new PagingDTO(new ArrayList<>(pagelist.getContent()), startPage, endPage, pre, next);
    }
}
