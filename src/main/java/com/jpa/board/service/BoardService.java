/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.jpa.board.service;

import com.jpa.board.dto.BoardDTO;
import com.jpa.board.entity.BoardEntity;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import com.jpa.board.repository.BoardRepository;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Optional;
import javax.servlet.ServletContext;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.io.Resource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.ResponseEntity;
import org.springframework.web.multipart.MultipartFile;

/**
 *
 * @author qntjd
 * 게시글 서비스를 제공하기 위한 클래스
 * 
 */
@Service
@RequiredArgsConstructor
@PropertySource("classpath:/system.properties")
public class BoardService {
    
    private final BoardRepository boardRepo;
    private final FileService fileService;
    
    @Value("${date.default.format}")
    private String dateformat;
    @Value("${board.file}")
    private String boardfile;
    @Autowired
    private ServletContext ctx;
    /**
     * 
     * @param board 작성한 게시글 정보
     * @param files 첨부파일 정보
     * @return save함수는 저장 성공한 객체를 반환하므로 객체 여부를 확인한다.
     * 게시글을 DB에 저장하기위한 함수
     * 
     */
    public boolean save(BoardDTO board, List<MultipartFile> files){
        BoardEntity boardEntity = BoardEntity.builder()
                .title(board.getTitle())
                .writer(board.getWriter())
                .contents(board.getContents())
                .fileCheck(files.get(0).isEmpty() ? 0 : 1)// 삼항 연산자를 사용하여 첨부된 파일이 있으면 1 없으면 0
                .build();
        BoardEntity save = boardRepo.save(boardEntity);// Entity만을 매개변수로 받음, 테이블에 insert하는 함수
        if(save.getFileCheck() == 1){
            //파일 저장 로직
            for(MultipartFile file : files){
                fileService.insertFile(ctx.getRealPath(boardfile), file, save.getId().toString());
            }
        }
        return save != null;
    }
    
    /**
     * 
     * @param pageable  페이지 객체
     * @param pageLimit 한페이지에 보여줄 글의 갯수
     * @return Entity 타입에서 DTO 타입으로 변환한 Board 정보 리스트
     * 게시글 전체 목록을 가져오기 위한 함수
     */
    public Page<BoardDTO> findAll(Pageable pageable, int pageLimit){
        
        //getPageNunber() 함수는 현재 선택된 페이지를 알려준다.
        // 정렬에 사용되는 기준은 Entity의 기준으로 변수 이름을 작성한다.
        Page<BoardEntity> entityList = boardRepo.findAll(PageRequest.of(pageable.getPageNumber() -1, pageLimit, Sort.by(Sort.Direction.DESC,"id")));
        
        // 날짜 출력을 위한 포맷
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern(dateformat);
        // Entity 타입에서 DTO 타입으로 전환
        Page<BoardDTO> dtoList = 
                entityList.map(entity -> BoardDTO.builder()
                            .id(entity.getId())
                            .title(entity.getTitle())
                            .writer(entity.getWriter())
                            .createDate(entity.getCreateDate().format(formatter))
                            .hits(entity.getHits())
                            .build());
        return dtoList;
    }
    
    /**
     * 
     * @param id 조회수를 올릴 게시물의 ID
     * @param request 쿠키 조작을 위한 request
     * @param response 쿠키 조작을 위한 response
     * 조회수 증가 함수
     */
    public void updateHits(Long id, HttpServletRequest request, HttpServletResponse response){
        // 접속 이력이 있는지 확인하기 위한 쿠키 목록
        Cookie oldCookie = null;
        Cookie[] cookies = request.getCookies();
        
        if (cookies != null) {
            for (Cookie cookie : cookies) {
                if (cookie.getName().equals("hit")) {
                    oldCookie = cookie;
                }
            }
        }
        if (oldCookie != null) {
            if (!oldCookie.getValue().contains("[" + id.toString() + "]")) {
                boardRepo.updateHits(id);
                oldCookie.setValue(oldCookie.getValue() + "_[" + id.toString() + "]");
                oldCookie.setPath("/");
                oldCookie.setMaxAge(365 * 24 * 60 * 60);
                response.addCookie(oldCookie);
            }
        } else {
            boardRepo.updateHits(id);
            Cookie newCookie = new Cookie("hit","[" + id.toString() + "]");
            newCookie.setPath("/");
            newCookie.setMaxAge(365 * 24 * 60 * 60);
            response.addCookie(newCookie);
        }
    }
    
    /**
     * 
     * @param id 조회할 게시글의 ID
     * @param request 조회수 증가를 위해 넘겨줄 request
     * @param response 조회수 증가를 위해 넘겨줄 response
     * @return 조회한 게시글의 객체정보
     */
    public BoardDTO findById(Long id, HttpServletRequest request, HttpServletResponse response){
        // 날짜 출력을 위한 포맷
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern(dateformat);

        //조회수를 올리기
        updateHits(id, request, response);
       
        //이후 상세정보 가져오기
        Optional<BoardEntity> entity = boardRepo.findById(id);
        if(!entity.isEmpty()){
            return BoardDTO.builder()
                    .id(entity.get().getId())
                    .title(entity.get().getTitle())
                    .writer(entity.get().getWriter())
                    .contents(entity.get().getContents())
                    .createDate(entity.get().getCreateDate().format(formatter))
                    .hits(entity.get().getHits())
                    .file(entity.get().getFileCheck()==1 ? fileService.searchFile(ctx.getRealPath(boardfile), entity.get().getId().toString()) : null)
                    .build();
        }else{
            return null;
        }
    }
    
    /**
     * 
     * @param filename 다운로드할 파일의 이름
     * @param lecid 첨부파일 정보를 가진 게시글 ID
     * @return 다운로드할 첨부파일 객체
     */
    public ResponseEntity<Resource> downloadFile(String filename, String lecid){
        return fileService.downloadFile(String.format("%s/%s", ctx.getRealPath(boardfile),lecid), filename);
    }
    
    public void delete(Long id){
        //첨부파일 삭제
        
        fileService.delFile(String.format("%s/%s", ctx.getRealPath(boardfile), id.toString()));
        //JPA의 deleteById는 void이므로 반환 값이 존재하지 않아 성공 여부를 확인이 힘들다
        //방법은 existsById와 같은 함수로 존재 여부를 판단하는 방법이 있다.
        //if(boardRepo.existsById(id)){
            boardRepo.deleteById(id);
        //    return true;
        //}
        //return false; -> 사용 시 반환 값 boolean으로 변경
    }
    
    public BoardDTO update(BoardDTO boardDTO, List<MultipartFile> files, String dellist, String remainlist){
        // 날짜 출력을 위한 포맷
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern(dateformat);
        int check = 1;
        if(files.get(0).isEmpty() && remainlist.equals("0")){
            check = 0;
        }
        BoardEntity boardEntity = BoardEntity.builder()
                .id(boardDTO.getId())
                .title(boardDTO.getTitle())
                .writer(boardDTO.getWriter())
                .contents(boardDTO.getContents())
                .createDate(LocalDateTime.parse(boardDTO.getCreateDate(),formatter))
                .hits(boardDTO.getHits())
                .fileCheck(check)//if문으로 삼항 연산 대처
                .build();
        BoardEntity entity = boardRepo.save(boardEntity);
        //삭제할 목록이 있으면 실행
        if(!dellist.equals("")){
            fileService.updateFile(String.format("%s/%s", ctx.getRealPath(boardfile), entity.getId()), dellist, remainlist);
        }
        //파일 저장
        if(entity.getFileCheck() == 1){
            //파일 저장 로직
            for(MultipartFile file : files){
                fileService.insertFile(ctx.getRealPath(boardfile), file, entity.getId().toString());
            }
        }
        
        return BoardDTO.builder()
                    .id(entity.getId())
                    .title(entity.getTitle())
                    .writer(entity.getWriter())
                    .contents(entity.getContents())
                    .createDate(entity.getCreateDate().format(formatter))
                    .hits(entity.getHits())
                    .file(entity.getFileCheck()==1 ? fileService.searchFile(ctx.getRealPath(boardfile), entity.getId().toString()) : null)
                    .build();
    }
}
