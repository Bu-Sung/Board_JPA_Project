/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.jpa.board.service;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.io.InputStreamResource;
import org.springframework.core.io.Resource;
import org.springframework.http.ContentDisposition;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

/**
 *
 * @author qntjd
 * 파일 처리를 위한 클래스
 */
@Service
@RequiredArgsConstructor
@PropertySource("classpath:/system.properties")
public class FileService {
    
    //폴더에 파일을 저장한다.
    public String insertFile(String realpath, MultipartFile file,String lecid){
        if(file.isEmpty() || "".equals(file.getOriginalFilename())){
            return "";
        }else{
            File baseDir = new File(realpath);// 각 파일 폴더
            File baseLecidDir = new File(String.format("%s/%s", realpath, lecid));// 각 강의 폴더
            
            if(!baseDir.exists()){
                baseDir.mkdir(); 
            }
            if(!baseLecidDir.exists()){
                baseLecidDir.mkdir();
            }
           
            File f = new File(baseLecidDir + File.separator + file.getOriginalFilename());
            try(BufferedOutputStream bos = new BufferedOutputStream(new FileOutputStream(f))){
                bos.write(file.getBytes());
                return lecid + File.separator + file.getOriginalFilename();
            }catch(IOException e){
                return e.getMessage();
            }
        }
    }
    
    //폴더 안에 파일 목록을 검색한다.
    public List<String> searchFile(String realpath, String lecid){
        List<String> fileNameList = new ArrayList<>();
        File[] fileList = new File(String.format("%s/%s", realpath, lecid)).listFiles();
        for(File file : fileList){
                fileNameList.add(file.getName());
        }
        return fileNameList;
    }
    
    //파일 다운로드
    public ResponseEntity<Resource> downloadFile(String url, String filename){
        HttpHeaders headers = new HttpHeaders();
        //파일의 Content-Type 찾기
        Path path = Paths.get(url + File.separator + filename);
        String contentType = null;
        try {
            contentType = Files.probeContentType(path);
        } catch (IOException e) {
        }

        //Http 헤더 생성
        headers.setContentDisposition(
                ContentDisposition.builder("attachment").filename(filename, StandardCharsets.UTF_8).build());
        headers.add(HttpHeaders.CONTENT_TYPE, contentType);

        //파일을 입력 스트림으로 만들어 내려받기 준비
        Resource resource = null;
        try {
            resource = new InputStreamResource(Files.newInputStream(path));
        } catch (IOException e) {
        }
        if (resource == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        return new ResponseEntity<>(resource, headers, HttpStatus.OK);
    }
    
    //폴더 삭제
    public void delFile(String path){
        File file = new File(path);
        File[] fileList = file.listFiles();
        if(fileList != null){
            for(File f : fileList){
                if(f.isFile()){
                    f.delete();
                }else{
                    delFile(f.getAbsolutePath());
                }
            }
            file.delete();
        }
    }
    
    //게시물을 수정할 때 삭제한 첨부파일 삭제
    public void updateFile(String path, String list, String remainlist){
        if(remainlist.equals("0")){
            delFile(path);
        }else{
            File file = new File(path);
            File[] fileList = file.listFiles();
            if(fileList != null){
                for(File f : fileList){
                    if(f.getName().equals(list)){
                        f.delete();
                    }
                }
            }
        }
    }
}
