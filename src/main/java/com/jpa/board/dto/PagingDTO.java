/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.jpa.board.dto;

import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Data;

/**
 *
 * @author qntjd
 */
@Data
@AllArgsConstructor
public class PagingDTO {
    List<Object> list;
    int start;
    int end; 
    boolean pre;
    boolean next;
}
