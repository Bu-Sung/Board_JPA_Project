/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.jpa.board.dto;

import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.web.multipart.MultipartFile;

/**
 *
 * @author qntjd
 * 게시글의 정보를 전달하기위한 DTO클래스
 */
@Data// @Getter, @Setter, @ToString
@AllArgsConstructor// 모든 변수를 매개변수로 하는 생성자
@NoArgsConstructor// 기본 생성자
@Builder
public class BoardDTO {
    private Long id; // 게시물 아이디
    private String writer; // 작성자
    private String title; // 제목
    private String contents; // 본문 내용
    private int hits; // 조회 수
    private String createDate; // 작성 날짜
    private List<String> file; // 파일명
}
