/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.jpa.board.repository;

import com.jpa.board.entity.BoardEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author qntjd
 * 게시글 Repository 인터페이스
 */
@Repository
public interface BoardRepository extends JpaRepository<BoardEntity, Long>{// 사용할 Entity 클래스 명과 PK의 타입
    /*
        JpaRepository를 상속 받았기 때문에 기본적인 메소드는 포함되어 있다.
        기본적으로 제공되는 메소드 이외에는 커스텀하여 정의해야한다.
    */
    // 사용자 정의 구문을 사용하는 함수에 필수 정의
    // SpringDataJPA가 관리하는 영속성 컨텍스트에서 데이터의 일관성 등을 위해 정의
    //수정, 삭제에 대해서 커스텀 메소드를 정의할 경우 선언 필수
    @Transactional
    @Modifying 
    /*
        커스텀 메소드를 제작하는데 사용할 쿼리를 작성 Entity클래스의 형태를 사용하여 작성하고
        : 이후의 값은 @Param을 통해 값을 설정(" "안에 있는 값과 이름이 동일)
        참고 - nativeQuery = true옵션을 적용하면 일반적인 DB 쿼리를 사용 가능
    */
    @Query(value="update BoardEntity b set b.hits=b.hits+1 where b.id=:id")
    void updateHits(@Param("id") Long id);
}
