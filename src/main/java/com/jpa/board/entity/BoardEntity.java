/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.jpa.board.entity;

import java.time.LocalDateTime;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.ColumnDefault;
import org.hibernate.annotations.CreationTimestamp;

/**
 *
 * @author qntjd
 * 게시글과 DB를 연결하기 위한 Entity 클래스
 */
@Data
@Entity
@Table(name="board")// 필수는 X, DB에 테이블 설정 가능
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class BoardEntity {
    @Id // Primary Key 지정
    @GeneratedValue(strategy = GenerationType.IDENTITY) // 자동 증가 어노테이션, DBMS마다 값이 다름
    private Long id; // 게시물 아이디
    private String writer; // 작성자
    private String title; // 제목
    private String contents; // 본문 내용
    private int hits; // 조회 수
    @CreationTimestamp // 데이터 삽입시 현재 날짜와 시간 입력
    private LocalDateTime createDate; // 작성 날짜
    @ColumnDefault("0")
    private int fileCheck; 
    
}
