/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.jpa.board.controller;

import com.jpa.board.dto.PagingDTO;
import com.jpa.board.service.BoardService;
import com.jpa.board.service.PagingService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

/**
 *
 * @author qntjd
 */
@Controller
@RequiredArgsConstructor
@Slf4j
public class SystemController {
    
    private final BoardService boardService;
    private final PagingService pagingService;
 
    @GetMapping("/")
    public String index(Model model,@PageableDefault(page=1) Pageable pageable){
        PagingDTO board = pagingService.getPaingBoardList(pageable);
        model.addAttribute("board", board);
        return "index";
    }
}
