/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.jpa.board.controller;

import com.jpa.board.dto.BoardDTO;
import com.jpa.board.service.BoardService;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.io.Resource;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

/**
 *
 * @author qntjd
 */
@Controller
@RequiredArgsConstructor // final이나 @Notnull이 붙어있는 변수의 생성자를 만들어준다.
@Slf4j
public class BoradController {
    
    private final BoardService boardService;
    
    // 게시글 작성 페이지로 이동
    @GetMapping("/save")
    public String boardSavePage(){
        return "save";
    }
    
    // 게시글 저장
    @PostMapping("/save.do")
    public String boardSave(@ModelAttribute BoardDTO board,@RequestParam(name="files") List<MultipartFile> file, RedirectAttributes attrs){
        /*
            @ModelAttribute 사용 시 이전 Form에서 넘겨 받은 name 값들 중
            해당 BoardDTO 객체의 변수 값과 명칭이 일치할 때
            해당 값을 맵핑 시켜준다.
        */
        
        if(boardService.save(board,file)){
            attrs.addFlashAttribute("msg", "게시글 등록에 성공하였습니다.");
        }else{
            attrs.addFlashAttribute("msg", "게시글 등록에 실패하였습니다.");
        } 
        return "redirect:/";
    }
    
    // 게시글 상세정보
    @GetMapping("/{id}")
    public String boardContent(Model model, @PathVariable Long id, HttpServletRequest request, HttpServletResponse response){
        model.addAttribute("content", boardService.findById(id, request, response));
        return "content";
    }
    
    // 첨부파일 다운로드
    @GetMapping("/download.do")
    public ResponseEntity<Resource> download(HttpServletRequest request){
        return boardService.downloadFile(request.getParameter("filename"), request.getParameter("lecid"));
    }
    
    // 게시글 삭제
    @GetMapping("/delete/{id}")
    public String deleteBoard(@PathVariable Long id, RedirectAttributes attrs){
        boardService.delete(id);
        attrs.addFlashAttribute("msg", "게시물이 삭제되었습니다.");
        return "redirect:/";
    }
    
    //게시글 수정 페이지로 이동
    @GetMapping("/update/{id}")
    public String boardUpdatePage(@PathVariable Long id, Model model, HttpServletRequest request, HttpServletResponse response){
        model.addAttribute("content", boardService.findById(id, request, response));
        return "update";
    }
    
    @PostMapping("/update/update.do")
    public String updateBoard(@ModelAttribute BoardDTO boardDTO, @RequestParam(name="files") List<MultipartFile> file, HttpServletRequest request, Model model){
        BoardDTO board = boardService.update(boardDTO, file, request.getParameter("dellist"), request.getParameter("remainlist"));
        model.addAttribute("board", board);
        return String.format("redirect:/%d", board.getId());
    }
}
