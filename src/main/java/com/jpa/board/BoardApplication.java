package com.jpa.board;

import java.io.IOException;
import static java.lang.StrictMath.log;
import java.util.Properties;
import org.springframework.beans.factory.config.PropertiesFactoryBean;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.core.io.ClassPathResource;

@SpringBootApplication
public class BoardApplication {

	public static void main(String[] args) {
		SpringApplication.run(BoardApplication.class, args);
	}
        
        @Bean(name = "systemProperties")
        public PropertiesFactoryBean systemProperties() {
            PropertiesFactoryBean bean = new PropertiesFactoryBean();
            bean.setLocation(new ClassPathResource("/system.properties"));
            try {
                Properties props = bean.getObject();
               //로그 남기기
            } catch (IOException ex) {
                //로그 남기기
            }
            return bean;
        }

}
