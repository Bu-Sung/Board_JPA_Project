<%-- 
    Document   : content
    Created on : 2023. 6. 30., 오후 12:03:04
    Author     : qntjd
--%>

<%-- 
    Document   : save
    Created on : 2023. 6. 30., 오전 2:39:09
    Author     : qntjd
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="">
        <meta name="author" content="">
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha2/dist/css/bootstrap.min.css" rel="stylesheet"
              integrity="sha384-aFq/bzH65dt+w6FI2ooMVUpc+21e0SRygnTpmBvdBgSdnuTN7QbdgL+OapgHtvPp" crossorigin="anonymous">
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.7.1/font/bootstrap-icons.css">
        <title>JPA게시판</title>

        <!-- Custom fonts for this template-->
        <link href="${pageContext.request.contextPath}/vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
        <link
            href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i"
            rel="stylesheet">

        <!-- Custom styles for this template-->
        <link href="${pageContext.request.contextPath}/assets/css/sb-admin-2.css" rel="stylesheet">
        <script>
            <c:if test="${!empty msg}">
            alert("${msg}");
            </c:if>
                
            function delBoard(id){
                alert(id);
            }
        </script>
    </head>

    <body id="page-top">

        <!-- Page Wrapper -->
        <div id="wrapper">

            <!-- Content Wrapper -->
            <div id="content-wrapper" class="d-flex flex-column">

                <!-- Main Content -->
                <div id="content">

                    <!-- Topbar -->
                    <%@include file="../jspf/topbar.jspf" %>
                    <!-- End of Topbar -->

                    <!-- Begin Page Content -->
                    <div class="container-fluid">

                        <!-- Page Heading -->
                        <div class="d-sm-flex align-items-center justify-content-between mb-4">
                            <h1 class="h3 mb-0 text-gray-800 ml-1">게시글 상세정보</h1>
                        </div>

                        <!-- Content Row -->
                        <div class="row">
                            <div class="card shadow mb-4">
                                <div class="card-body">

                                    <div class="ml-1 mr-1">
                                        <label class="h3">${content.getTitle()}</label>
                                    </div>
                                    <div class=" d-flex justify-content-between ml-1 mr-1">
                                        <div class="float-left">
                                            <span >${content.getWriter()}</span>
                                        </div>
                                        <div class="float-end">
                                            <span>${content.getCreateDate()}</span>
                                            <span class="text-gray-800">조회수 : ${content.getHits()}</span>
                                        </div>
                                    </div>
                                    <hr class="sidebar-divider border-dark">
                                    <div class="ml-1 mr-1">
                                        ${content.getContents()}
                                    </div>
                                    <c:if test="${!empty content.getFile().get(0)}">
                                        <hr class="sidebar-divider border-dark">
                                        <div class="ml-1 mr-1 row">
                                            <div class="col-2 pl-0">
                                                <label>첨부파일</label>
                                            </div>
                                            <div class="col-10">
                                                <c:forEach items="${content.getFile()}" var="list">
                                                    <p class="m-0"><a href="download.do?filename=${list}&lecid=${content.getId()}">${list}</a></p>
                                                </c:forEach>
                                            </div>
                                        </div>
                                    </c:if>
                                    <hr class="sidebar-divider border-dark">
                                    <div class="float-end mb-3">
                                        <button type="button" class="btn btn-sm btn-primary" onclick="location.href=`update/${content.getId()}`">수정</button>
                                        <!--토글을 이용해서 팝업 창을 커스텀 onclick 속성을 사용해서 JS alert 메시지를 통해 구현도 가능-->
                                        <button type="button" class="btn btn-sm btn-primary" data-toggle="modal" data-target="#delModal" >삭제</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /.container-fluid -->

                </div>
                <!-- End of Main Content -->

                <!-- Footer -->
                <%@include file="../jspf/footer.jspf" %>
                <!-- End of Footer -->

            </div>
            <!-- End of Content Wrapper -->

        </div>
        <!-- End of Page Wrapper -->

        <!-- Scroll to Top Button-->
        <a class="scroll-to-top rounded" href="#page-top">
            <i class="fas fa-angle-up"></i>
        </a>

        <!-- Logout Modal-->
        <%@include file="../jspf/modal.jspf" %>

        <!-- Bootstrap core JavaScript-->
        <script src="${pageContext.request.contextPath}/vendor/jquery/jquery.min.js"></script>
        <script src="${pageContext.request.contextPath}/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

        <!-- Core plugin JavaScript-->
        <script src="${pageContext.request.contextPath}/vendor/jquery-easing/jquery.easing.min.js"></script>

        <!-- Custom scripts for all pages-->
        <script src="${pageContext.request.contextPath}/js/sb-admin-2.js"></script>

        <!-- Page level plugins -->
        <script src="${pageContext.request.contextPath}/vendor/chart.js/Chart.min.js"></script>

        <!-- Page level custom scripts -->
        <script src="${pageContext.request.contextPath}/js/demo/chart-area-demo.js"></script>
        <script src="${pageContext.request.contextPath}/js/demo/chart-pie-demo.js"></script>

    </body>

</html>