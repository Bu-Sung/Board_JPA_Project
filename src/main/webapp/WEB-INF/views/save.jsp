<%-- 
    Document   : save
    Created on : 2023. 6. 30., 오전 2:39:09
    Author     : qntjd
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="">
        <meta name="author" content="">
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha2/dist/css/bootstrap.min.css" rel="stylesheet"
              integrity="sha384-aFq/bzH65dt+w6FI2ooMVUpc+21e0SRygnTpmBvdBgSdnuTN7QbdgL+OapgHtvPp" crossorigin="anonymous">
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.7.1/font/bootstrap-icons.css">
        <title>JPA게시판</title>

        <!-- Custom fonts for this template-->
        <link href="${pageContext.request.contextPath}/vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
        <link
            href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i"
            rel="stylesheet">

        <!-- Custom styles for this template-->
        <link href="${pageContext.request.contextPath}/assets/css/sb-admin-2.css" rel="stylesheet">
        <script>
            <c:if test="${!empty msg}">
            alert("${msg}");
            </c:if>
        </script>
    </head>

    <body id="page-top">

        <!-- Page Wrapper -->
        <div id="wrapper">

            <!-- Content Wrapper -->
            <div id="content-wrapper" class="d-flex flex-column">

                <!-- Main Content -->
                <div id="content">

                    <!-- Topbar -->
                    <%@include file="../jspf/topbar.jspf" %>
                    <!-- End of Topbar -->

                    <!-- Begin Page Content -->
                    <div class="container-fluid">

                        <!-- Page Heading -->
                        <div class="d-sm-flex align-items-center justify-content-between mb-4">
                            <h1 class="h3 mb-0 text-gray-800 ml-1">게시글 작성하기</h1>
                        </div>

                        <!-- Content Row -->
                        <div class="row">
                            <div class="card shadow mb-4">
                                <form action="save.do" method="post" autocomplete="off" enctype="multipart/form-data">
                                    <div class="card-body">
                                        <table class="table">
                                            <tr>
                                                <th scope="row">
                                                    <label class="text-black">제목</label>
                                                </th>
                                                <td>
                                                    <div class="row">
                                                        <div class="col-md-7">
                                                            <div class="input-group input-group-outline">
                                                                <input type="text" class="form-control" name="title" placeholder="제목을 입력해주세요" required>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <th scope="row">
                                                    <label class="text-black">작성자</label>
                                                </th>
                                                <td>
                                                    <div class="row">
                                                        <div class="col-md-4">
                                                            <div class="input-group input-group-outline">
                                                                <input type="text" class="form-control" name="writer" placeholder="작성자를 입력해주세요" required>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <th scope="row">
                                                    <label class="text-black">내용</label>
                                                </th>
                                                <td>
                                                    <div class="row">
                                                        <div class="col-md-7">
                                                            <div class="input-group input-group-outline">
                                                                <textarea name="contents" id="contents" class="form-control" placeholder="내용을 입력해주세요" cols="30" rows="20" required></textarea>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <th scope="row">
                                                    <label class="text-black">첨부파일</label>
                                                </th>
                                                <td>
                                                    <div class="row">
                                                        <div class="col-md-7">
                                                            <div class="input-group input-group-outline">
                                                                <input type="file" name="files" class="form-control" placeholder="파일을 첨부해주세요" multiple>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </td>
                                            </tr>
                                        </table>
                                        <div class="float-end mb-3">
                                            <button type="button" class="btn btn-secondary" onclick="location.href = '${pageContext.request.contextPath}'">뒤로가기</button>
                                            <button type="submit" class="btn btn-primary" >작성하기</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>

                    </div>
                    <!-- /.container-fluid -->

                </div>
                <!-- End of Main Content -->

                <!-- Footer -->
                <%@include file="../jspf/footer.jspf" %>
                <!-- End of Footer -->

            </div>
            <!-- End of Content Wrapper -->

        </div>
        <!-- End of Page Wrapper -->

        <!-- Scroll to Top Button-->
        <a class="scroll-to-top rounded" href="#page-top">
            <i class="fas fa-angle-up"></i>
        </a>

        <!-- Logout Modal-->
        <%@include file="../jspf/modal.jspf" %>

        <!-- Bootstrap core JavaScript-->
        <script src="${pageContext.request.contextPath}/vendor/jquery/jquery.min.js"></script>
        <script src="${pageContext.request.contextPath}/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

        <!-- Core plugin JavaScript-->
        <script src="${pageContext.request.contextPath}/vendor/jquery-easing/jquery.easing.min.js"></script>

        <!-- Custom scripts for all pages-->
        <script src="${pageContext.request.contextPath}/js/sb-admin-2.js"></script>

        <!-- Page level plugins -->
        <script src="${pageContext.request.contextPath}/vendor/chart.js/Chart.min.js"></script>

        <!-- Page level custom scripts -->
        <script src="${pageContext.request.contextPath}/js/demo/chart-area-demo.js"></script>
        <script src="${pageContext.request.contextPath}/js/demo/chart-pie-demo.js"></script>
    </body>

</html>
