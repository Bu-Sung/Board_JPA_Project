<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="">
        <meta name="author" content="">
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha2/dist/css/bootstrap.min.css" rel="stylesheet"
              integrity="sha384-aFq/bzH65dt+w6FI2ooMVUpc+21e0SRygnTpmBvdBgSdnuTN7QbdgL+OapgHtvPp" crossorigin="anonymous">
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.7.1/font/bootstrap-icons.css">
        <title>JPA게시판</title>

        <!-- Custom fonts for this template-->
        <link href="${pageContext.request.contextPath}/vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
        <link
            href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i"
            rel="stylesheet">

        <!-- Custom styles for this template-->
        <link href="${pageContext.request.contextPath}/assets/css/sb-admin-2.css" rel="stylesheet">
        <script>
            <c:if test="${!empty msg}">
                alert("${msg}");
            </c:if>
        </script>
    </head>

    <body id="page-top">

        <!-- Page Wrapper -->
        <div id="wrapper">

            <!-- Content Wrapper -->
            <div id="content-wrapper" class="d-flex flex-column">

                <!-- Main Content -->
                <div id="content">

                    <!-- Topbar -->
                    <%@include file="../jspf/topbar.jspf" %>
                    <!-- End of Topbar -->

                    <!-- Begin Page Content -->
                    <div class="container-fluid">

                        <!-- Page Heading -->
                        <div class="d-sm-flex align-items-center justify-content-between mb-4">
                            <h1 class="h3 mb-0 text-gray-800 ml-1">게시판 목록</h1>
                            <a href="save" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm mr-1"></i> 게시글 작성하기</a>
                        </div>

                        <!-- Content Row -->
                        <div class="row">
                            <div class="card shadow mb-4">
                                <div class="card-body">
                                    <div class="input-group mb-2 col-6 float-end p-0">
                                        <input type="text" class="form-control bg-light border-0 small" placeholder="Search for..."
                                               aria-label="Search" aria-describedby="basic-addon2">
                                        <div class="input-group-append">
                                            <button class="btn btn-primary" type="button">
                                                <i class="fas fa-search fa-sm"></i>
                                            </button>
                                        </div>
                                    </div>
                                    <table class="table">
                                        <thead>
                                        <colgroup>
                                            <col style="width: 10%;">
                                            <col style="width: 50%;">
                                            <col style="width: 10%;">
                                            <col style="width: 20%;">
                                            <col style="width: 10%;">
                                        </colgroup>
                                        <tr>
                                            <th class="text-center">No.</th>
                                            <th>제목</th>
                                            <th class="text-center">작성자</th>
                                            <th class="text-center">작성날짜</th>
                                            <th class="text-center">조회수</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                            <c:forEach items="${board.getList()}" var="list">
                                                <tr>
                                                    <td class="text-center">${list.getId()}</td>
                                                    <td><a href="${list.getId()}">${list.getTitle()}</a></td>
                                                    <td class="text-center">${list.getWriter()}</td>
                                                    <td class="text-center">${list.getCreateDate()}</td>
                                                    <td class="text-center">${list.getHits()}</td>
                                                </tr>
                                            </c:forEach>


                                        </tbody>
                                    </table>
                                    <div class="col-auto ">
                                        <div class="dataTable_paginate paging_simple_numbers d-flex justify-content-center" id="dataTable_paginate">
                                            <ul class="pagination">
                                                <c:if test="${board.isPre()}">
                                                    <li class="paginate_button page-item previous"><a href="${pageContext.request.contextPath}?page=${param.page-1}" aria-controls="dataTable" class="page-link">Previous</a></li>
                                                 </c:if>
                                                <c:forEach begin="${board.getStart()}" end="${board.getEnd()}" var="page">
                                                    <li class="paginate_button page-item"><a href="${pageContext.request.contextPath}?page=${page}" aria-controls="dataTable" class="page-link">${page}</a></li>
                                                </c:forEach>
                                                    <c:if test="${board.isNext()}">
                                                    <li class="paginate_button page-item next"><a href="${pageContext.request.contextPath}?page=${param.page+1}" aria-controls="dataTable" class="page-link">Next</a></li>
                                                </c:if>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /.container-fluid -->
                </div>
                <!-- End of Main Content -->
                <!-- Footer -->
                <%@include file="../jspf/footer.jspf" %>
                <!-- End of Footer -->
            </div>
            <!-- End of Content Wrapper -->
        </div>
        <!-- End of Page Wrapper -->
        <!-- Scroll to Top Button-->
        <a class="scroll-to-top rounded" href="#page-top">
            <i class="fas fa-angle-up"></i>
        </a>
        <!-- Logout Modal-->
        <%@include file="../jspf/modal.jspf" %>

        <!-- Bootstrap core JavaScript-->
        <script src="${pageContext.request.contextPath}/vendor/jquery/jquery.min.js"></script>
        <script src="${pageContext.request.contextPath}/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

        <!-- Core plugin JavaScript-->
        <script src="${pageContext.request.contextPath}/vendor/jquery-easing/jquery.easing.min.js"></script>

        <!-- Custom scripts for all pages-->
        <script src="${pageContext.request.contextPath}/js/sb-admin-2.js"></script>

        <!-- Page level plugins -->
        <script src="${pageContext.request.contextPath}/vendor/chart.js/Chart.min.js"></script>

        <!-- Page level custom scripts -->
        <script src="${pageContext.request.contextPath}/js/demo/chart-area-demo.js"></script>
        <script src="${pageContext.request.contextPath}/js/demo/chart-pie-demo.js"></script>

    </body>

</html>